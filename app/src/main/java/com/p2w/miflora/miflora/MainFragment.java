package com.p2w.miflora.miflora;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.GridLayoutAnimationController;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.ContentValues.TAG;

import static maes.tech.intentanim.CustomIntent.customType;


public class MainFragment extends android.support.v4.app.Fragment {
    private Firebase mRef;
    FloatingActionButton fab;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    JSONArray groups = null;
    private String uid,keyId;
    private GridView gridView;
    DeviceAdapter deviceAdapter;
    FrameLayout frameLayout;
    ArrayList<Device> devices = new ArrayList<Device>();
    SwipeRefreshLayout swipeRefreshLayout;


    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDevice();
            }
        });
        frameLayout = (FrameLayout) view.findViewById(R.id.frameLayout);
        fab = (FloatingActionButton) view.findViewById(R.id.fab_add);
        gridView = (GridView) view.findViewById(R.id.devicegridview);
        Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.grid_item_anim);
        GridLayoutAnimationController controller = new GridLayoutAnimationController(animation, .2f, .2f);
        gridView.setLayoutAnimation(controller);

        deviceAdapter = new DeviceAdapter(getActivity(), devices, MainFragment.this);

        gridView.setAdapter(deviceAdapter);
        loadDevice();

        // Inflate the layout for this fragment
        return view;
    }

    public void loadDevice(){
        if(isOnline()) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                // User is signed in
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getActivity(), AddDeviceActivity.class);
                        startActivity(i);

                        customType(getActivity(), "bottom-to-up");
                    }
                });

                uid = user.getUid();

                mRef = new Firebase("https://miflora-p2w.firebaseio.com/users/");
                mFirebaseInstance = FirebaseDatabase.getInstance();
                mFirebaseDatabase = mFirebaseInstance.getReference("users/" + uid);
                keyId = mFirebaseDatabase.getKey();


                mFirebaseInstance.getReference("users/" + uid).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        devices.clear();

                        Log.e(TAG, "Key updated");


                        //HashMap<String, String> map = new HashMap<String, String>();
                        //DataSnapshot userId = dataSnapshot;
                        Iterable<DataSnapshot> key1 = dataSnapshot.getChildren();
                        for (final DataSnapshot device : key1) {
                            Log.d("Devices", "" + device);
                            Key c = device.getValue(Key.class);

                            final String keyGet = c.getKey().toString();
                            final String macGet = c.getMac().toString();
                            final String titleGet = c.getTitle().toString();

                            final Device d = new Device();
                            d.setKey(keyGet);
                            d.setMac(macGet);
                            d.setTitle(titleGet);

                            //bypass Mirai
                            String date = "2018-01-18";
                            String time = "18:42";
                            //DatabaseReference mRef  = FirebaseDatabase.getInstance().getReference().child("data").child(macGet).child(date).child(time);
                            //ValueEventListener eventListener = new ValueEventListener() {
                            mFirebaseInstance.getReference("data/" + macGet + "/" + date + "/" + time).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    Iterable<DataSnapshot> value1 = dataSnapshot.getChildren();
                                    for (DataSnapshot device : value1) {
                                        Log.d("Devices Data", "" + device);
                                        if (device.getKey().equals("fertility")) {
                                            String fertGet = String.valueOf(device.getValue(Long.class));
                                            d.setFert(fertGet);
                                        } else if (device.getKey().equals("lux")) {
                                            String luxGet = String.valueOf(device.getValue(Long.class));
                                            d.setLux(luxGet);
                                        } else if (device.getKey().equals("moisture")) {
                                            String moistGet = String.valueOf(device.getValue(Long.class));
                                            d.setMoist(moistGet);
                                        } else if (device.getKey().equals("temperature")) {
                                            String tempGet = String.valueOf(device.getValue(Long.class));
                                            d.setTemper(tempGet);
                                        } else if (device.getKey().equals("battery")) {
                                            String battGet = String.valueOf(device.getValue(Long.class));
                                            d.setBatt(battGet);
                                        } else if (device.getKey().equals("picture")) {
                                            String picGet = String.valueOf(device.getValue(String.class));
                                            d.setUrlPic(picGet);
                                        }
                                    }

                                    Log.e(TAG, "" + value1 + " " + macGet);

                                    devices.add(d);
                                    deviceAdapter.notifyDataSetChanged();
                                    swipeRefreshLayout.setRefreshing(false);
                                }

                                @Override
                                public void onCancelled(DatabaseError error) {
                                    // Failed to read value
                                    Log.e(TAG, "Failed to read date value.", error.toException());
                                }
                            });
                            //mRef.addValueEventListener(eventListener);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.e(TAG, "Failed to read key value.", error.toException());
                    }
                });
            } else {
                // No user is signed in
            }
        }
        else {
            final Snackbar connError = Snackbar.make(frameLayout, "Connection Error", Snackbar.LENGTH_INDEFINITE);
            View snackbarView = connError.getView();
            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            connError.setActionTextColor(Color.WHITE);
            connError.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    connError.dismiss();
                    loadDevice();
                }
            });
            connError.show();
        }
    }
    public void trashClicked(final int position) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // User is signed in
            if (isOnline()) {
                if(devices.isEmpty()){
                    deviceAdapter.notifyDataSetChanged();
                }
                else {
                    String uid = user.getUid();
                    mFirebaseInstance = FirebaseDatabase.getInstance();
                    mFirebaseDatabase = mFirebaseInstance.getReference("users/" + uid);
                    //ref.child(uid).child(mac).removeValue();
                    String keyTrash = devices.get(position).getKey();
                    String macTrash = devices.get(position).getMac();
                    mFirebaseDatabase.child(keyTrash).setValue(null);
                    Log.e("Delete", "" + keyTrash + " " + macTrash);
                    Toast.makeText(getActivity().getApplicationContext(), "Removed", Toast.LENGTH_SHORT).show();
                }
            }
            else{
                final Snackbar connError = Snackbar.make(frameLayout, "Connection Error", Snackbar.LENGTH_INDEFINITE);
                View snackbarView = connError.getView();
                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                connError.setActionTextColor(Color.WHITE);
                connError.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        connError.dismiss();
                        trashClicked(position);
                    }
                });
                connError.show();
            }
        }
    }
    public void chartClicked(int position) {
        Intent intent = new Intent(getActivity(), ChartTodayActivity.class);
        Bundle extra = new Bundle();
        extra.putString("mac",devices.get(position).getMac());
        intent.putExtras(extra);
        Toast.makeText(getActivity(),"Loading",Toast.LENGTH_SHORT).show();
        getActivity().startActivity(intent);
        customType(getActivity(), "left-to-right" );
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

}
