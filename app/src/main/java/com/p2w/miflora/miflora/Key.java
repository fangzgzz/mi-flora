package com.p2w.miflora.miflora;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Fanno on 23/11/2017.
 */

public class Key {

    private String key;
    private String mac;
    private String title;

    public Key() {
    }

    public Key(String key, String mac, String title) {
        this.key = key;
        this.mac = mac;
        this.title = title;
    }

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String name) {
        this.key = name;
    }


    @JsonProperty("mac")
    public String getMac() {
        return mac;
    }

    @JsonProperty("mac")
    public void setMac(String mac) {
        this.mac = mac;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

}