package com.p2w.miflora.miflora;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeviceAdapter extends BaseAdapter {
    Context c;
    ArrayList<Device> devices;

    private View vi;
    private TableRow chartV;

    private String fertGet, luxGet, moistGet, temperGet, batteryGet, mac, key, pictureGet;
    private TextView fertV, luxV, moistV, temperV, batteryV, txtId, keyV, macV;

    private ImageView trashV,pictureV;
    private MainFragment fragment;

    private Context mContext;
    public LayoutInflater inflater=null;

    public DeviceAdapter(Context c, ArrayList<Device> devices, MainFragment fragment) {
        this.c = c;
        this.devices = devices;
        this.fragment = fragment;
        inflater = LayoutInflater.from(c);
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(int position) {
        return devices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        vi=convertView;
        if(convertView==null) {
            vi = inflater.inflate(R.layout.device_layout, null);
        }

        this.chartV = (TableRow) vi.findViewById(R.id.chart);
        this.trashV = (ImageView) vi.findViewById(R.id.trash);
        this.txtId = (TextView)vi.findViewById(R.id.deviceName);
        this.fertV = (TextView) vi.findViewById(R.id.fert);
        this.luxV = (TextView) vi.findViewById(R.id.lux);
        this.moistV = (TextView) vi.findViewById(R.id.moist);
        this.temperV = (TextView) vi.findViewById(R.id.temper);
        this.batteryV = (TextView) vi.findViewById(R.id.battery);
        this.keyV = (TextView) vi.findViewById(R.id.keyV);
        this.macV = (TextView) vi.findViewById(R.id.macV);
        this.pictureV = (ImageView) vi.findViewById(R.id.picture);

        //HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);
        final Device d = (Device) this.getItem(position);

        final String id = d.getTitle().toString();
        this.key = (String) d.getKey().toString();
        this.txtId.setText(id);
        this.keyV.setText(key);
        this.mac = (String) d.getMac().toString();
        this.macV.setText(mac);
        this.fertGet = (String) d.getFert().toString();
        if(fertGet==null) fertGet="-";
        this.fertV.setText(fertGet);
        this.luxGet = (String) d.getLux().toString();
        if(luxGet==null) luxGet="-";
        this.luxV.setText(luxGet);
        this.moistGet = (String) d.getMoist().toString();
        if(moistGet==null) moistGet="-";
        this.moistV.setText(moistGet);
        this.temperGet = (String) d.getTemper().toString();
        if(temperGet==null) temperGet="-";
        this.temperV.setText(temperGet);
        this.batteryGet = (String) d.getBatt().toString();
        if(batteryGet == null) batteryGet="-";
        this.batteryV.setText(batteryGet+"%");
        this.pictureGet = (String) d.getUrlPic();
        Glide.with(c).load(""+pictureGet).centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.logo_chart).into(pictureV);

        this.chartV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.chartClicked(position);
            }
        });
        trashV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.trashClicked(position);
            }
        });

        return vi;
    }
}
