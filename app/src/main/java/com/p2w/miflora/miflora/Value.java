package com.p2w.miflora.miflora;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Fanno on 23/11/2017.
 */

public class Value {

    private String fertility;
    private String lux;
    private String moisture;
    private String temperature;

    public Value() {
    }

    public Value(String fertility, String lux, String moisture, String temperature) {
        this.fertility = fertility;
        this.lux = lux;
        this.moisture = moisture;
        this.temperature = temperature;
    }

    @JsonProperty("fertility")
    public String getFert() {
        return fertility;
    }

    @JsonProperty("fertility")
    public void setFert(String fertility) {
        this.fertility = fertility;
    }


    @JsonProperty("lux")
    public String getLux() {
        return lux;
    }

    @JsonProperty("lux")
    public void setLux(String mac) {
        this.lux = mac;
    }

    @JsonProperty("moisture")
    public String getMoist() {
        return moisture;
    }

    @JsonProperty("moisture")
    public void setMoist(String moisture) {
        this.moisture = moisture;
    }

    @JsonProperty("temperature")
    public String getTemper() {
        return temperature;
    }

    @JsonProperty("temperature")
    public void setTemper(String temperature) {
        this.temperature = temperature;
    }
}