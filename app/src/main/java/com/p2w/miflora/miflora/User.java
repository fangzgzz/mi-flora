package com.p2w.miflora.miflora;

/**
 * Created by Fanno on 23/11/2017.
 */

public class User {

    private String id;
    private String key;
    public User() {
    }

    public User(String key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String name) {
        this.key = name;
    }

}