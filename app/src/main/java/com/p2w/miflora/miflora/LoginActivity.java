package com.p2w.miflora.miflora;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.firebase.client.Firebase;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {
    private CallbackManager callbackManager;
    private ImageView loginB_fb, loginB_google,call_twitter;
    protected TwitterLoginButton login_twitter;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;
    private String provider = "";
    private static int RC_SIGN_IN = 100;
    private ImageView login_logo;
    private ProgressBar pbar;
    //Add YOUR Firebase Reference URL instead of the following URL

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(getString(R.string.com_twitter_sdk_android_CONSUMER_KEY), getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)))
                .debug(true)
                .build();
        Twitter.initialize(config);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Firebase.setAndroidContext(this);
        Twitter.initialize(this);

        loginB_fb   = (ImageView) findViewById(R.id.login_button_fb);
        loginB_google = (ImageView) findViewById(R.id.login_button_google);
        call_twitter = (ImageView) findViewById(R.id.call_button_twitter);
        login_twitter = (TwitterLoginButton) findViewById(R.id.login_button_twitter);
        login_logo = (ImageView) findViewById(R.id.login);
        pbar = (ProgressBar) findViewById(R.id.progress_bar);

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                pbar.setVisibility(View.GONE);
                login_logo.setVisibility(View.VISIBLE);
                loginB_fb.setImageResource(R.drawable.logo_facebook);
                loginB_google.setImageResource(R.drawable.logo_google);
                call_twitter.setImageResource(R.drawable.logo_twitter);
                if(user!=null){
                    Toast.makeText(LoginActivity.this, "Welcome "+user.getDisplayName(),Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
//                    Bundle extra = new Bundle();
//                    extra.putString("provider", mGoogleApiClient.toString());
//                    intent.putExtras(extra);
                    startActivity(intent);
                    finish();
                }
                else {
                    Toast.makeText(LoginActivity.this, "Sign In Failed",Toast.LENGTH_SHORT).show();
                }

            }
        };

        callbackManager = CallbackManager.Factory.create();


        loginB_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginB_fb.setImageResource(R.drawable.logo_facebook_clicked);
                if (isOnline()) {
                    provider = "facebook";
                    LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email", "public_profile"));
                }
                else{
                    final Snackbar connError = Snackbar.make(findViewById(R.id.activityLayout), "Connection Error", Snackbar.LENGTH_INDEFINITE);
                    View snackbarView = connError.getView();
                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    connError.setActionTextColor(Color.WHITE);
                    connError.setAction("DISMISS", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            connError.dismiss();
                            loginB_fb.setImageResource(R.drawable.logo_facebook);
                        }
                    });
                    connError.show();
                }
            }
        });
        loginB_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginB_google.setImageResource(R.drawable.logo_google_clicked);
                if(isOnline()){
                    provider="google";
                    signInGoogle();
                }
                else{
                    final Snackbar connError = Snackbar.make(findViewById(R.id.activityLayout), "Connection Error", Snackbar.LENGTH_INDEFINITE);
                    View snackbarView = connError.getView();
                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    connError.setActionTextColor(Color.WHITE);
                    connError.setAction("DISMISS", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            connError.dismiss();
                            loginB_google.setImageResource(R.drawable.logo_google);
                        }
                    });
                    connError.show();
                }
            }
        });
        call_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call_twitter.setImageResource(R.drawable.logo_twitter_clicked);
                if(isOnline()){
                    provider="twitter";
                    login_twitter.performClick();
                }
                else{
                    final Snackbar connError = Snackbar.make(findViewById(R.id.activityLayout), "Connection Error", Snackbar.LENGTH_INDEFINITE);
                    View snackbarView = connError.getView();
                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    connError.setActionTextColor(Color.WHITE);
                    connError.setAction("DISMISS", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            connError.dismiss();
                            call_twitter.setImageResource(R.drawable.logo_twitter);
                        }
                    });
                    connError.show();
                }
            }
        });

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code

                        // LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {...});
                        signInWithFacebook(loginResult.getAccessToken());
                        //Intent loggedIn = new Intent(LoginActivity.this, HomeActivity_BackUp.class);
                         //startActivity(loggedIn);
                         //LoginActivity.this.finish();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                }
        );

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this , new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        login_twitter.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                pbar.setVisibility(View.VISIBLE);
                login_logo.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, "Signing In",Toast.LENGTH_SHORT).show();
                handleTwitterSession(result.data);
            }

            @Override
            public void failure(TwitterException exception) {
            }
        });
    }

    private void signInGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(provider.toString().equals("facebook")) {
            pbar.setVisibility(View.VISIBLE);
            login_logo.setVisibility(View.GONE);
            Toast.makeText(LoginActivity.this, "Signing In",Toast.LENGTH_SHORT).show();
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        else if (provider.toString().equals("google")){
            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                pbar.setVisibility(View.VISIBLE);
                login_logo.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, "Signing In",Toast.LENGTH_SHORT).show();
                if (result.isSuccess()) {
                    // Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = result.getSignInAccount();
                    firebaseAuthWithGoogle(account);

                } else {
                    // Google Sign In failed, update UI appropriately
                    Toast.makeText(LoginActivity.this, "Sign In Failed",Toast.LENGTH_SHORT).show();
                    provider=null;
                }
            }
        }
        else{
            login_twitter.onActivityResult(requestCode, resultCode, data);
        }
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        mAuth.addAuthStateListener(mAuthListener);
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        if (mAuthListener != null) {
//            mAuth.removeAuthStateListener(mAuthListener);
//        }
//    }

    private void signInWithFacebook(AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Sign In failed.",
                                    Toast.LENGTH_SHORT).show();
                        }else{

                            mAuth.addAuthStateListener(mAuthListener);
                        }
                    }
                });
    }
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }


                        mAuth.addAuthStateListener(mAuthListener);
                    }
                });
    }

    private void handleTwitterSession(TwitterSession session) {
        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            mAuth.addAuthStateListener(mAuthListener);
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(LoginActivity.this, "Sign In failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }
    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
}

