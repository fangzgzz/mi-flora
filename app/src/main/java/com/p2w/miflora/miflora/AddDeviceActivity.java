package com.p2w.miflora.miflora;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import static maes.tech.intentanim.CustomIntent.customType;

public class AddDeviceActivity extends AppCompatActivity {
    private EditText macV,titleV;
    private String key,mac,title;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private Button submit;
    private DatabaseReference mRef;
    private FirebaseUser user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        macV = (EditText) findViewById(R.id.mac_edit);
        titleV = (EditText) findViewById(R.id.title_edit);
        submit = (Button) findViewById(R.id.submit_button);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(titleV.getText().toString().equals("")||macV.getText().toString().equals("")){
                    Toast.makeText(AddDeviceActivity.this,"Complete the form first", Toast.LENGTH_SHORT).show();
                }
                else if (titleV.getText().toString().equals("")&&macV.getText().toString().equals("")){
                    Toast.makeText(AddDeviceActivity.this,"Complete the form first", Toast.LENGTH_SHORT).show();
                }
                else {
                    mac = macV.getText().toString();
                    start();
                    //ref = new Firebase("https://miflora-p2w.firebaseio.com/users/");

                }


                };
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        customType(AddDeviceActivity.this, "up-to-bottom" );
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
    protected void start(){
        if(isOnline()){
            mRef = FirebaseDatabase.getInstance().getReference();

            final DatabaseReference checkR = mRef.child("device");
            checkR.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild(mac)) {
                        mAuth = FirebaseAuth.getInstance();

                        user = FirebaseAuth.getInstance().getCurrentUser();
                        if (user != null) {
                            String uid = user.getUid();
                            String title = titleV.getText().toString();


//                            Create a new User and Save it in Firebase database

                            Firebase mRef = new Firebase("https://miflora-p2w.firebaseio.com/users/");
                            String key = mRef.child(uid).push().getKey();
                            Key key1 = new Key(key, mac, title);
                            mRef.child(uid).child(key).setValue(key1);
                            Toast.makeText(AddDeviceActivity.this, "Add success", Toast.LENGTH_SHORT).show();
                            //Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                            //startActivity(intent);
                            customType(AddDeviceActivity.this, "up-to-bottom" );
                            finish();
                        } else {
                            Toast.makeText(AddDeviceActivity.this, "Add failed.", Toast.LENGTH_SHORT);
                        }
                    } else {
                        Toast.makeText(AddDeviceActivity.this, "Add failed. Device not exist", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        else {
            final Snackbar connError = Snackbar.make(findViewById(R.id.activityLayout), "Connection Error", Snackbar.LENGTH_INDEFINITE);
            View snackbarView = connError.getView();
            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            connError.setActionTextColor(Color.WHITE);
            connError.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    connError.dismiss();
                    start();
                }
            });
            connError.show();
        }
    }
}
