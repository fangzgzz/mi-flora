package com.p2w.miflora.miflora;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.util.EncodingUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static maes.tech.intentanim.CustomIntent.customType;

public class ChartAverageActivity extends AppCompatActivity {
    private String chartAvg = "https://us-central1-miflora-p2w.cloudfunctions.net/average";
    private WebView webView;
    private String mac, macPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        start();
        //webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_chart_avg, menu);

        MenuItem todayD = menu.findItem(R.id.today);

        todayD.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(ChartAverageActivity.this, ChartTodayActivity.class);
                Bundle extra = new Bundle();
                extra.putString("mac",mac);
                intent.putExtras(extra);
                Toast.makeText(ChartAverageActivity.this,"Loading",Toast.LENGTH_SHORT).show();
                startActivity(intent);
                customType(ChartAverageActivity.this, "left-to-right" );
                finish();
                return false;
            }
        });

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        customType(ChartAverageActivity.this, "right-to-left" );
        finish();
    }
    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
    protected void start(){
        if(isOnline()){
            mac= getIntent().getExtras().getString("mac");

            webView = (WebView) findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true);
            try {
                macPost = "mac="+ URLEncoder.encode(mac, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Log.e("Yourapp", "UnsupportedEncodingException");
            }
            //webView.postUrl(chartToday, EncodingUtils.getBytes(macPost, "BASE64"));
            webView.postUrl(chartAvg, macPost.getBytes());
            Log.e("Post: ",""+macPost);
            Log.e("Post: ",""+macPost.getBytes());
            Log.e("Post: ",""+EncodingUtils.getBytes(macPost, "BASE64"));
            webView.setHorizontalScrollBarEnabled(false);
        }
        else{
            final Snackbar connError = Snackbar.make(findViewById(R.id.activityLayout), "Connection Error", Snackbar.LENGTH_INDEFINITE);
            View snackbarView = connError.getView();
            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            connError.setActionTextColor(Color.WHITE);
            connError.setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    connError.dismiss();
                    start();
                }
            });
            connError.show();
        }
    }
}
